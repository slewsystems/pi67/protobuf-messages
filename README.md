# AuDEc gRPC

The gRPC service used to communicate to AuDEc Engine via [gRPC services](https://grpc.io/) and [Protobuf messages](https://developers.google.com/protocol-buffers/). If you are looking to control AuDEc Engine remotely, via TCP, it is suggested you utilize the GraphQL API provided by AuDEc Server.

## Table of Contents

1. [Usage](#usage)
1. [gRPC Service Methods](#service-methods)
   1. [Player Control (PlayerCtl)](#player-control-playerctl)
   1. [Logger Control (LoggerCtl)](#logger-control-loggerctl)
   1. [Engine Control (EngineCtl)](#engine-control-enginectl)
   1. [Stat Peeking (StatPeek)](#stats)
   1. [Stat Streaming (StatStream)](#stats)
1. [Development](#development)

---

## Usage

// TODO

## Service Methods

The `AudecService` module contains all the methods available to control AuDEc Engine. Below you can find documentation for each of these methods, their inputs (request messages) and outputs (response messages). This service is part of the `audec` package and is namespaced as such.

### Player Control (PlayerCtl)

Use the `PlayerCtl` method to change player settings. By default, all fields are optional, omitting a field will result in no change in AuDEc Engine behavior.

#### Request Message

|     | Field         | Type              | Description                                                                                                  | Default         |
| --- | ------------- | ----------------- | ------------------------------------------------------------------------------------------------------------ | --------------- |
| 1   | `mode`        | `enum PlayerMode` | Set play mode.<br/>`0`: No Change, `1`: Play, `2`: Pause, `3`: Stop                                          | `0`: No Change  |
| 2   | `volume`      | `int32`           | Set volume.<br/>_A negative value will result in no change._                                                 | `-1`: No Change |
| 3   | `sdp_content` | `string`          | Contents of a SDP file.<br/>_An empty string will result in no change._                                      | `""`: No Change |
| 4   | `channel_map` | `repeated int32`  | Pairs of input <> output channel number values (starting at 1).<br/>_Ex. [input1, output1, input2, output3]_ | `[]`: No Change |

#### Response Message

|     | Field      | Type                   | Description                                                         |
| --- | ---------- | ---------------------- | ------------------------------------------------------------------- |
| 1   | `response` | `enum RequestResponse` | Resulting response code.<br/>`0`: Success, `1`: Error, `2`: Pending |

### Logger Control (LoggerCtl)

Use the `LoggerCtl` method to change logger settings.

#### Request Message

// TODO

#### Response Message

|     | Field      | Type                   | Description                                                         |
| --- | ---------- | ---------------------- | ------------------------------------------------------------------- |
| 1   | `response` | `enum RequestResponse` | Resulting response code.<br/>`0`: Success, `1`: Error, `2`: Pending |

### Engine Control (EngineCtl)

Use the `EngineCtl` method to change which operating mode to operate in. It is suggested you first switch into your intended mode then configure it. Switching modes will reset the current state and settings to default values.

#### Request Message

|     | Field  | Type              | Description                                                                 | Default        |
| --- | ------ | ----------------- | --------------------------------------------------------------------------- | -------------- |
| 1   | `mode` | `enum EngineMode` | Set operating mode.<br/>`0`: No Change, `1`: Idle, `2`: Player, `3`: Logger | `0`: No Change |

#### Response Message

|     | Field      | Type                   | Description                                                        |
| --- | ---------- | ---------------------- | ------------------------------------------------------------------ |
| 1   | `response` | `enum RequestResponse` | Resulting response code.<br>`0`: Success, `1`: Error, `2`: Pending |

### Stats

There are two ways to retrieve stats; by *peek*ing or *stream*ing: `StatPeek` and `StatStream` respectively. Both of these methods take in the same request message and return the same response message. As the name suggests, `StatStream` will return a message stream while `StatPeek` will return just a single response message.

##### Request Message

// TODO

#### Response Message

// TODO

## Development

This repo is broken down into sub packages depending on the programming language you wish to target. Below is a list of the currently supported targets. The specific build configuration and scripts for each of these can be found in their respective folder within the `packages` folder.

| Language | Package name    | Architecture | OS            |
| -------- | --------------- | ------------ | ------------- |
| C++      | audec_grpc_cpp  | x86_64, arm  | macOS, Debian |
| NodeJS   | audec_grpc_node | _any_        | _any_         |

### Compile Protobufs

These steps are only needed if you are looking to re-compile the gRPC service or messages. You do not have to compile it yourself if you are targeting a supported system where we offer prebuilt libraries (see table above) via our private registries.

### Compile gRPC for C++

Generated code will be placed into the `build` directory of the `audec_grpc_cpp` package.

```bash
dip provision
dip compile_cpp
```

### Compiling gRPC for NodeJS

Generated code will be placed into the `dist` directory of the `audec_grpc_node` package.

```bash
dip provision
dip compile_node
```
