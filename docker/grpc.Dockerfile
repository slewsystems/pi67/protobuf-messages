FROM debian:buster

ENV DEBIAN_FRONTEND="noninteractive"

ARG GRPC_RELEASE_TAG="v1.28.1"
ARG BAZEL_RELEASE_TAG="3.5.0"

# use bash (not sh) when executing bazel compilation scripts
SHELL ["/bin/bash", "-c"]

# update apt repos for latest packages when installing
RUN apt-get update -qq

# not sure why we need to manually set CAs... but without this we get SSL cert issues:
# curl: (60) SSL certificate problem: unable to get local issuer certificate
# https://stackoverflow.com/a/31060428
RUN apt-get -y -qq --no-install-recommends \
    install ca-certificates && \
    update-ca-certificates
ENV CURL_CA_BUNDLE="/etc/ssl/certs/ca-certificates.crt"

RUN apt-get -y -qq --no-install-recommends install \
    build-essential \
    git \
    zip \
    unzip \
    curl \
    wget \
    python3

RUN ln -s /usr/bin/python3 /usr/bin/python

# ----- BAZEL -----

RUN git clone --quiet \
    https://github.com/koenvervloesem/bazel-on-arm.git /var/local/git/bazel \
    --depth=1

# install jdk and other system requirements ourselves rather than use the
# make requirements script provided by bazel-on-arm since it has some bugs in it
RUN apt-get -y -qq --no-install-recommends install \
    openjdk-11-jdk-headless \
    libatomic1 && \
    update-java-alternatives -s java-1.11.0-openjdk-armhf

RUN cd /var/local/git/bazel && \
    ./scripts/build_bazel.sh "$BAZEL_RELEASE_TAG" && \
    make install

# ----- GRPC -----

RUN git clone --quiet \
    https://github.com/grpc/grpc /var/local/git/grpc \
    --branch ${GRPC_RELEASE_TAG} \
    --depth=1 \
    --recurse-submodules

# we install python3-distutils because you get an error compiling grpc
# https://github.com/pypa/pip/issues/5367#issuecomment-386864941
RUN apt-get -y -qq --no-install-recommends install \
    python3-distutils \
    autoconf \
    libtool \
    pkg-config \
    clang \
    libc++-dev

RUN cd /var/local/git/grpc && \
    bazel build :all

# ----- NODE -----

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get -y -qq --no-install-recommends install nodejs

RUN npm install yarn -g

# ----- AUDEC -----

RUN apt-get -y -qq --no-install-recommends install \
    python3 \
    python3-pip \
    ninja-build \
    clang

RUN pip3 install meson -q