#!/bin/bash
set -e
set -o pipefail

# Clean up previous files
rm -rf "$BUILD_DIR"

# configure any extra stuff
if [[ $# -eq 0 ]]; then
  echo 'No configuration arguments passed, using default build options'
fi

# Build the ninja build scripts
meson "$BUILD_DIR" "$SOURCE_DIR" "$@"

# compile it!
ninja -C "$BUILD_DIR" -v
